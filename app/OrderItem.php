<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    protected $table = 'order_items';
    protected $primaryKey = 'id';

    protected $fillable = ['discount', 'quantity', 'price', 'product_id', 'sum', 'order_id'];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}

<?php

namespace App\Http\Resources;

use App\Buyer;
use App\OrderItem;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        $orderItems = OrderItem::where('order_id', '=', $this->id)->get();
        $buyer = Buyer::find($this->buyer_id);

        return [
            'id' => $this->id,
            'orderDate' => $this->date,
            'orderSum' => $orderItems->sum('sum')/100,
            'orderItems' =>
                $orderItems->map(function ($orderItem) {
                    $product = Product::find($orderItem->product_id);
                    return [
                        'productName' => $product->name,
                        'productQty' => $orderItem->quantity,
                        'productPrice' => $orderItem->price/100,
                        'productDiscount' => $orderItem->discount . '%',
                    ];
                })
            ,
            'buyer' => [
                'buyerFullName' => $buyer->name . ' ' . $buyer->surname,
                'buyerAddress' => $buyer->country . ' ' . $buyer->city . ' ' . $buyer->addressLine,
                'buyerPhone' => $buyer->phone,
            ]
        ];
    }
}

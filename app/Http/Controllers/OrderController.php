<?php

namespace App\Http\Controllers;

use App\Http\Resources\OrderResource;
use App\Order;
use App\OrderItem;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Input;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Collection
     */
    public function index(): Collection
    {
        $orders = Order::all();

        return $orders->map(function ($order){
            return new OrderResource($order);
        });
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request): Response
    {
        $data = Input::all();

        $orderData = [
            'buyer_id' => $data['buyerId'],
            'date' => now()->format('Y-m-d'),
        ];

        $order = Order::create($orderData);

        $this->populateOrderWithOrderItems($order->id, $data['orderItems']);

        $order->save();

        return new Response($order);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return OrderResource
     */
    public function show(int $id): OrderResource
    {
        return new OrderResource(Order::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, int $id): Response
    {
        $order = Order::find($id);

        if (!$order) return new Response([
            'result' => 'fail',
            'message' => 'order not found'
        ]);

        $data = Input::all();

        $order->date = now()->format('Y-m-d');

        OrderItem::where('order_id', '=', $order->id)->delete();

        $this->populateOrderWithOrderItems($order->id, $data['orderItems']);

        $order->save();

        return new Response(new OrderResource($order));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return array
     */
    public function destroy(int $id): array
    {
        OrderItem::where('order_id', '=', $id)->delete();

        $result = Order::destroy($id);

        return ['result' => $result ? 'success' : 'fail'];
    }

    /**
     * Remove all orders from storage by buyer_id
     *
     * @param int $buyerId
     * @return array
     */
    public function deleteByBuyer(int $buyerId): array
    {
        $orders = Order::where('buyer_id', '=', $buyerId)->get();

        $orders->map(function ($order){
            OrderItem::where('order_id', '=', $order->id)->delete();
        });

        $result = Order::where('buyer_id', '=', $buyerId)->delete();

        return ['result' => $result ? 'success' : 'fail'];
    }

    /**
     * @param int $orderId
     * @param array $orderItems
     * @return void
     */
    private function populateOrderWithOrderItems(int $orderId, array $orderItems): void
    {
        foreach ($orderItems as $orderItem) {
            $product = Product::find($orderItem['productId']);

            $orderItemData = [
                'product_id' => $orderItem['productId'],
                'quantity' => $orderItem['productQty'],
                'discount' => $orderItem['productDiscount'],
                'price' => $product->price,
                'sum' => $this->calculateSum($product->price, $orderItem['productDiscount'], $orderItem['productQty']),
                'order_id' => $orderId,
            ];

            $orderItemModel = OrderItem::create($orderItemData);
            $orderItemModel->save();
        }
    }

    /**
     * @param int $price
     * @param int $discount
     * @param int $qty
     * @return float
     */
    private function calculateSum(int $price, int $discount, int $qty): float
    {
        return ($price - ($price * ($discount / 100))) * $qty;
    }
}

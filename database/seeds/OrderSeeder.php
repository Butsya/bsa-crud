<?php

use Illuminate\Database\Seeder;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = \App\Product::all();
        factory(\App\Buyer::class, 10)->create()
            ->each(function (\App\Buyer $buyer) use ($products) {
                $buyer->orders()->saveMany(
                    factory(\App\Order::class, 5)
                        ->make(['buyer_id' => null])
                );
            });

        $orders = \App\Order::all();

        $orders->map(
            function (\App\Order $order) use ($products) {
                $order->orderItems()->saveMany(
                    factory(\App\OrderItem::class, 5)
                        ->make(['order_id' => $order->id])
                        ->each(function (\App\OrderItem $orderItem) use ($products) {
                            $product = $products->shuffle()->shuffle()->first();

                            $orderItem->product_id = $product->id;
                            $orderItem->price = $product->price;
                            $orderItem->sum = $this->calculateSum(
                                $product->price, $orderItem->discount, $orderItem->quantity
                            );

                        })
                );

            }
        );
    }

    /**
     * @param int $price
     * @param int $discount
     * @param int $qty
     * @return float
     */
    private function calculateSum(int $price, int $discount, int $qty): float
    {
        return ($price - ($price * ($discount / 100))) * $qty;
    }
}

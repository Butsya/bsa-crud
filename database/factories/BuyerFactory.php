<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Buyer;
use Faker\Generator as Faker;

$factory->define(Buyer::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'surname' => $faker->name,
        'country' => $faker->country,
        'city' => $faker->city,
        'addressLine' => $faker->address,
        'phone' => $faker->phoneNumber,
    ];
});
